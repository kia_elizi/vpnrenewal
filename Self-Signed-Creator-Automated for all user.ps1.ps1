﻿# ========================================================================================== #
# ////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////////////// #
# ========================================================================================== #
#                                 ***************************                                #
#                                 ***  ESO IT Department  ***                                #
#                                 ***************************                                #
#                       Designed And Implemented By IT Department (ESO)                      #
# ========================================================================================== #
#   First Editor(s): Kia Salimi Elizi                                                        #
#    Last Editor(s): -                                                                       #
# ------------------------------------------------------------------------------------------ #
#  Script's Version: 1.0                                                                     #
#     Script's Type: Private           # (Value: Public; Semi-Public; Semi-Private; Private) #
#     Script's Env.: Production                              # (Value: IAT, UAT, Production) #
#     First Edition: 25/01/2022                                                              #
#      Last Edition:                                                                         #
# ------------------------------------------------------------------------------------------ #
#  PS Compatibility: 5.0+                                                                    #
# Need To Be Signed: No                                                   # (Value: Yes; No) #
#   Req. Components: Ionic.Zip.dll PS2EXE (https://github.com/MScholtes/PS2EXE)              #
#   Req. PS Modules: NA                                                                      #
# ------------------------------------------------------------------------------------------ #
#           Subject: Automated VPN certificate creator                                       #
# ------------------------------------------------------------------------------------------ #
#        Comment(s): Creating Azure VPN certificate covers both users and gateway            #
#              Note: Credential will be randomly generated and put in the targeted           #
#                    directory                                                               #
# ========================================================================================== #
# ////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////////////// #
# ========================================================================================== #

###################################
## Defining Predefined Variables ##
###################################

Add-Type -AssemblyName 'System.Web'
$strTargetEnv = "Dearborn", "AMS", "SJANT", "Sales"
$strUsers = 'Kia','Matte','Hug','Gayle','Martin','Dan','Andy'
$strExportPath = "C:\tmp\Certs\"
$strStableDate = (Get-Date -Format "yyyy-MM")
[System.Reflection.Assembly]::LoadFrom("D:\Media\Ionic.Zip.dll");



## Export Prepration ##
If ( -not (Test-Path -LiteralPath ($strExportPath) -PathType Container ) )
{New-Item -Path ($strExportPath) -ItemType Directory -ErrorAction Stop}

If ( -not (Test-Path -LiteralPath ($strExportPath + '\Zips') -PathType Container ) )
{New-Item -Path ($strExportPath + '\Zips') -ItemType Directory -ErrorAction Stop}

Foreach($strUser in $strUsers)
{
    if ( -not (Test-Path -LiteralPath ($strExportPath + $strUser) -PathType Container ) )
    {New-Item -Path ($strExportPath + $strUser) -ItemType Directory -ErrorAction Stop}
}
#######################
## Root Cert Creator ##
#######################
    Foreach($strCustomer in $strTargetEnv)
    {
    ## Root Folder Creator ##
        if ( -not (Test-Path -LiteralPath ($strExportPath + '\Root') -PathType Container ) )
        {New-Item -Path ($strExportPath + '\Root') -ItemType Directory -ErrorAction Stop}
    ## Root Cert Subject ##
        $strSubjectCreator = $strCustomer + "-Root-Cert-" + $strStableDate
    ## Root Cert Creation ##
        New-SelfSignedCertificate -Type Custom -KeySpec Signature -Subject $strSubjectCreator `
        -KeyExportPolicy Exportable -HashAlgorithm sha256 -KeyLength 2048 -CertStoreLocation "Cert:\CurrentUser\My" `
        -KeyUsageProperty Sign -KeyUsage CertSign -NotAfter (Get-Date).AddYears(2)
    ## Fetching the Root Cert ##
        $strTMPPath = "Cert:\CurrentUser\My\" + $strSubjectCreator
        $strRootThumbprint = (ls "Cert:\CurrentUser\My\" | Where-Object{$_.Subject -eq "CN=$strSubjectCreator"}).Thumbprint
        $strRootThumbprint = "Cert:\CurrentUser\My\" + $strRootThumbprint

    ## Exporting Root Cert + Pvt ##
        if ( -not (Test-Path -LiteralPath ($strExportPath + '\Root' + '\PvtKey') -PathType Container ) )
        {New-Item -Path ($strExportPath + '\Root' + '\PvtKey') -ItemType Directory -ErrorAction Stop}

        ## Root PASSW ##
            $strPasstmp = [System.Web.Security.Membership]::GeneratePassword(25, 5)
            $strPasstmp > ($strExportPath + '\Root' + '\PvtKey' + '\' + $strCustomer + '-PASS.TXT')
            $strRootPass = ConvertTo-SecureString -String $strPasstmp -Force -AsPlainText
        ## Export Command ##
            Export-PfxCertificate -Cert $strRootThumbprint `
            -FilePath ($strExportPath + '\Root' + '\PvtKey' + '\' + $strSubjectCreator + '.pfx')`
            -Password $strRootPass

    ## Exporting Root Cert ##
        Export-Certificate -Cert $strRootThumbprint -FilePath ($strExportPath + '\Root' + '\' + $strSubjectCreator + '.cer') -Type CERT
    ## Converting to Base64 ##
        certutil -encode ($strExportPath + '\Root' + '\' + $strSubjectCreator + '.cer') `
        ($strExportPath + '\Root' + '\Base64' + $strSubjectCreator + '.cer')

    }
#########################
## CLient Cert Creator ##
#########################
    Foreach($strCustomer in $strTargetEnv)
    {
    ## Clients Folder Creator ##
    #    if ( -not (Test-Path -LiteralPath ($strExportPath + $strCustomer + '\Clients') -PathType Container ) )
    #    {New-Item -Path ($strExportPath + $strCustomer + '\Clients') -ItemType Directory -ErrorAction Stop}

    ## Client Cert Creation ##
        Foreach ($strUser in $strUsers)
        {
            ## Fetching Proper Root ##
            $strSubjectCreator = $strCustomer + "-Root-Cert-" + $strStableDate
            $strRootThumbprint = (ls "Cert:\CurrentUser\My\" | Where-Object{$_.Subject -eq "CN=$strSubjectCreator"}).Thumbprint
            $strRootThumbprint = "Cert:\CurrentUser\My\" + $strRootThumbprint

            ## Client Subject Creator ##
            Write-Host $strUser
            $strDnsName = $strCustomer + '-' + $strUser + 'Client-Cert-' + $strStableDate
            Write-Host $strDnsName

            New-SelfSignedCertificate -Type Custom -DnsName $strDnsName -KeySpec Signature `
            -Subject "CN=$strDnsName" -KeyExportPolicy Exportable -HashAlgorithm sha256 `
            -KeyLength 2048 -CertStoreLocation "Cert:\CurrentUser\My" -Signer $strRootThumbprint `
            -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.2")

            ## Fetching the Client Cert ##
                $strTMPClientThumbprint = (ls "Cert:\CurrentUser\My\" | Where-Object{$_.Subject -eq "CN=$strDnsName"}).Thumbprint
                $strTMPClientThumbprint = "Cert:\CurrentUser\My\" + $strTMPClientThumbprint

            ## Exporting Client Cert + Pvt ##
                if ( -not (Test-Path -LiteralPath ($strExportPath + $strUser + '\' + $strCustomer) -PathType Container ) )
                {New-Item -Path ($strExportPath + $strUser + '\' + $strCustomer) -ItemType Directory -ErrorAction Stop}

            ## Client PASSW ##
                $strPasstmp = [System.Web.Security.Membership]::GeneratePassword(25, 5)
                $strPasstmp > ($strExportPath + $strUser + '\' + $strCustomer + '\PASS.TXT')
                $strClientPass = ConvertTo-SecureString -String $strPasstmp -Force -AsPlainText

            ## Export Command ##
                Export-PfxCertificate -Cert $strTMPClientThumbprint `
                -FilePath ($strExportPath + $strUser + '\' + $strCustomer + '\' + $strDnsName + '.pfx')`
                -Password $strClientPass

        ###########################
        ## CLient Script Creator ##
        ###########################
        $strValue = @'
# ========================================================================================== #
#   First Editor(s): Kia Salimi Elizi                                                        #
#    Last Editor(s): -                                                                       #
# ------------------------------------------------------------------------------------------ #
#  Script's Version: 1.0                                                                     #
#     Script's Type: Private           # (Value: Public; Semi-Public; Semi-Private; Private) #
#     Script's Env.: Production                              # (Value: IAT, UAT, Production) #
#     First Edition: 25/01/2022                                                              #
#      Last Edition:                                                                         #
# ------------------------------------------------------------------------------------------ #
#  PS Compatibility: 5.0+                                                                    #
# Need To Be Signed: No                                                   # (Value: Yes; No) #
#   Req. Components: NA                                                                      #
#   Req. PS Modules: NA                                                                      #
# ------------------------------------------------------------------------------------------ #
#           Subject: Automated Cert installer (Client Side)                                  #
# ------------------------------------------------------------------------------------------ #
#        Comment(s): Removing old certificate and install the new ones                       #
#              Note: this will be pacakged with certs and password and should be             #
#                    executed on client machine                                              #
# ========================================================================================== #
# ////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////////////// #
# ========================================================================================== #
###############################
## Removing Old certificates ##
###############################

ls "Cert:\CurrentUser\My\" | Where-Object{$_.subject -Like "*2020-02"} | Remove-Item
ls "Cert:\CurrentUser\My\" | Where-Object{$_.subject -Like "*2021-02"} | Remove-Item

######################
## Adding new Certs ##
######################

Add-Type -AssemblyName 'System.Web'
$strTargetEnv = "Dearborn", "AMS", "SJANT", "Sales"
$strUser = '
'@ + $strUser + @'
'
Foreach($strCustomer in $strTargetEnv)
{
    ## Cert Path Creator ##
    $strCertPath = '.\' + $strCustomer + '\' + $strCustomer + '-' + $strUser + 'Client-Cert-2022-01.pfx'
    Write-Host $strCertPath
    $strPassPath = '.\' + $strCustomer + '\PASS.TXT'
    Write-Host $strPassPath
    $strPassword = Get-Content $strPassPath
    $strSecPass = ConvertTo-SecureString -AsPlainText $strPassword -Force
    Import-PfxCertificate -Exportable -Password $strSecPass -FilePath $strCertPath -CertStoreLocation "Cert:\CurrentUser\My\" -Confirm
}
Pause
'@
            ## PowerShell Script creator ##
            Set-Content -Path ($strExportPath + $strUser + '\' + 'Client-Cert-installer' + '.ps1') -Value $strValue -Force
            ## .EXE creator ##
            Invoke-ps2exe ($strExportPath + $strUser + '\' + 'Client-Cert-installer' + '.ps1') ($strExportPath + $strUser + '\' + 'Client-Cert-installer' + '.exe')
            ## Zip File Creator ##
            $strPasstmp = [System.Web.Security.Membership]::GeneratePassword(25, 5)
            $strPasstmp > ($strExportPath + '\Zips' + '\' + $strUser + '-PASS.TXT')
            $ZipCreator =  new-object Ionic.Zip.ZipFile
            $ZipCreator.Encryption = [Ionic.Zip.EncryptionAlgorithm]::PkzipWeak
            $ZipCreator.Password = $strPasstmp
            $ZipCreator.AddDirectory($strExportPath + $strUser)
            $ZipFileName =  $strExportPath + '\Zips' + '\' + $strUser + '-bundle.zip'
            $ZipCreator.Save($ZipFileName)
        }
    }