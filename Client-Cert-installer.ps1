﻿# ========================================================================================== #
#   First Editor(s): Kia Salimi Elizi                                                        #
#    Last Editor(s): -                                                                       #
# ------------------------------------------------------------------------------------------ #
#  Script's Version: 1.0                                                                     #
#     Script's Type: Private           # (Value: Public; Semi-Public; Semi-Private; Private) #
#     Script's Env.: Production                              # (Value: IAT, UAT, Production) #
#     First Edition: 25/01/2022                                                              #
#      Last Edition:                                                                         #
# ------------------------------------------------------------------------------------------ #
#  PS Compatibility: 5.0+                                                                    #
# Need To Be Signed: No                                                   # (Value: Yes; No) #
#   Req. Components: NA                                                                      #
#   Req. PS Modules: NA                                                                      #
# ------------------------------------------------------------------------------------------ #
#           Subject: Automated Cert installer (Client Side)                                  #
# ------------------------------------------------------------------------------------------ #
#        Comment(s): Removing old certificate and install the new ones                       #
#              Note: this will be pacakged with certs and password and should be             #
#                    executed on client machine                                              #
# ========================================================================================== #
# ////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////////////////////// #
# ========================================================================================== #
###############################
## Removing Old certificates ##
###############################

ls "Cert:\CurrentUser\My\" | Where-Object{$_.subject -Like "*2020-02"} | Remove-Item

######################
## Adding new Certs ##
######################

Add-Type -AssemblyName 'System.Web'
$strTargetEnv = "Dearborn", "IMT", "SJANT", "Sales"
$strUser = 'Kia'#,'Matte','Hug','Gayle','Martin'

Foreach($strCustomre in $strTargetEnv)
{
    ## Cert Path Creator ##
    $strCertPath = '.\' + $strCustomre + '\' + $strCustomre + '-' + $strUser + 'Client-Cert-2022-01.pfx'
    Write-Host $strCertPath
    $strPassPath = '.\' + $strCustomre + '\PASS.TXT'
    Write-Host $strPassPath
    $strPassword = Get-Content $strPassPath
    $strSecPass = ConvertTo-SecureString -AsPlainText $strPassword -Force
    Import-PfxCertificate -Exportable -Password $strSecPass -FilePath $strCertPath -CertStoreLocation "Cert:\CurrentUser\My\" -Confirm
}
Pause